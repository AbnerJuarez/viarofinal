/**
	This function randomize the port of user profile
*/
$("#userPort").ready(function(){
	var numb = Math.floor((Math.random() * 31) + 1);
	$("#userPort").css("background-image", "url('../resources/patterns/"+numb+".png')");
});

/**
	* This function determinate the navbar's link switch the type of user
*/
$("#nav").ready(function(){
	if(sessionStorage["master"]!=undefined){
		$(".desk").css("visibility", "visible");
		$("#aname").attr("href", "dashboardMaster.html");
	}
});