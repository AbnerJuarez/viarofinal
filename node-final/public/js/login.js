var app = angular.module("appLogin", []);
/**
	* This function control the login in the app
	* $scope {Object} This value control the binding between views and model
	* $http {Object} This object is used to web service calls
*/
app.controller('ctrlLogin', function($scope, $http){
	$scope.login = function(){
		var user = {
			correo: $scope.correo,
			password: $scope.password
		}
		var userLogged = {}

		$http.post("api/final/login/maestro?correo="+user.correo+"&password="+user.password, {})
			.success(function(data){
				userLogged = data;

				if(isEmpty(userLogged)){
					$http.post("api/final/login/alumno?correo="+user.correo+"&password="+user.password, {})
					.success(function(data){
						userLogged = data;

						if(isEmpty(userLogged)){
							//alert("Invalid credentials");
							Materialize.toast('Invalid User, try again.', 10000);
						}else{
							chargeUser(userLogged, "Alumno");
						}
					})
					.error(function(err){
						console.log(err);
					});
				}else{
					chargeUser(userLogged, "Master");
				}
			})
			.error(function(err){
				console.log(err);
			});	
	}
});

/**
	* This function verifies that an object isn't empty
	* @param {Object} obj Object to verify
	* @return boolean
*/
function isEmpty(obj){
	var cont = 0;
	var empt = true;
	for(var attr in obj){
		cont++;
		if(cont>0){
			return false;
		}
	}
	return true;
}

/**
	* @param {Object} user The object with the information of login
	* @param {String} page Next page's string for url
*/
function chargeUser(user, page){
	sessionStorage.setItem("nombre", user.nombre);
	if(page === "Master"){
		sessionStorage.setItem("id", user.idmaestro);
		sessionStorage.setItem("master", 1);
	}else if(page === "Alumno"){
		sessionStorage.setItem("id", user.idalumno);
		sessionStorage.setItem("idalumno", user.idalumno);
	}
	sessionStorage.setItem("correo", user.correo);
	//alert(sessionStorage["correo"] +";"+sessionStorage["nombre"] +";"+ sessionStorage["id"]);
	window.location.href = "/dashboard"+page+".html";
}