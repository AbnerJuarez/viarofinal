/**
	@module AppMaster Angular app for master domain
*/
var app = angular.module("appMaster", []);
/**
	@controller ctrlMaster Angular controller for control the master domain
*/
app.controller('ctrlMaster', function($scope, $http){
	//List of students
	$scope.alumnos = [];
	//List of courses
	$scope.cursos = [];
	//Temporal list for courses
	$scope.tempCursos = [];
	//Temporal list of courses to assign
	$scope.assignCursos = [];

	//Master url to Web Service calls
	var mastUri = "/api/final/";

	//Temporal student to update
	$scope.upalumno = {};
	//User logged
	$scope.logged = {};

	if(sessionStorage["idalumno"] != undefined){
		chargeLogged();
	}

	//Student Object to insert in data model 
	$scope.alumno = {
		nombre: "",
		password: "",
		correo: "",
		imagen: "",
		idmaestro: 0
	}
	//Teacher Object to insert in data model
	$scope.master = {
		nombre: "",
		correo: "",
		password: ""
	}
	//Course Object to insert in data model
	$scope.course = {
		nombre: "",
		ciclo: 0
	}

	$http.get("/api/final/alumno/all/"+sessionStorage["id"], {})
		.success(function(data){
			$scope.alumnos = data;

			for(var al in $scope.alumnos){

				ajaxHelper(mastUri+"alumno/course/"+$scope.alumnos[al].idalumno, "GET").success(function(data){
					$scope.alumnos[al]["notes"] = data;
				});
			}
		})
		.error(function(err){
			console.log(err);
		});

	/**
		@param {String} uri String with the url call
		@param {String} method Method to execute the call
		@param {Object} data Object with the information required for call
		@return {Object} Return an Ajax object to execute calls
	*/
	function ajaxHelper(uri, method, data){
		return $.ajax({
			type: method,
			async: false,
			url: uri,
			datatype: 'json',
			contentType: 'application/json',
			data: data ? JSON.stringify(data) : null
		}).fail(function(err){
			console.log(err);
		});
	}

	/**
		This function charge the student to edit from data model using his id
		@param {Integer} idAlumno Student's id for get his information
	*/
	$scope.chargeUpdate = function(idAlumno){
		$scope.upalumno = {};

		ajaxHelper(mastUri+"alumno/"+idAlumno, "GET").success(function(item){
			$scope.upalumno = item;

			ajaxHelper(mastUri+"alumno/course/"+idAlumno, "GET").success(function(data){
				$scope.upalumno["notes"] = data
				//console.log(angular.toJson($scope.upalumno.notes));
			});
		});

		$('#modal-update').openModal();
	}

	/**
		This function update the information of student charged previously
	*/
	$scope.actualizarAlumno = function(){
		var lastStudent = {};
		ajaxHelper(mastUri+"alumno/"+$scope.upalumno.idalumno, "GET").success(function(item){
			lastStudent = item;
		});

		ajaxHelper(mastUri+"alumno/"+$scope.upalumno.idalumno, "PUT", $scope.upalumno).success(function(item){
			$scope.upalumno.nombre = item.nombre;
			$scope.upalumno.password = item.password;
			$scope.upalumno.correo = item.correo;
			$scope.upalumno.imagen = item.imagen;
			for(var note in $scope.upalumno.notes){
				//console.log($scope.upalumno.notes[note].idalumnocurso);
				var alc = $scope.upalumno.notes[note];
				var promedio = (alc.unidad1 + alc.unidad2 + alc.unidad3 + alc.unidad4 + alc.unidad5) / 5;
				if(promedio >= 75 && promedio <= 100){
					$scope.upalumno.notes[note].estado = "Gano";
				}else if(promedio < 75 && promedio >=55 ){
					$scope.upalumno.notes[note].estado = "Recuperacion";
				}else if(promedio < 55 && promedio >=1){
					$scope.upalumno.notes[note].estado = "Perdio";
				}
				ajaxHelper(mastUri+"alumnocurso/"+$scope.upalumno.notes[note].idalumnocurso, "PUT", $scope.upalumno.notes[note]).success(function(ite){
					$scope.upalumno.notes[note] = ite;
					//console.log(angular.toJson(ite));
				});
			}
		});
		$.each($scope.alumnos, function(index, val){
			if($scope.alumnos[index].idalumno === $scope.upalumno.idalumno){
				$scope.alumnos[index] = $scope.upalumno;
			}
		});
		
	}

	$scope.idTemp = 0;

	/**
		This function gets the courses that a student have assigned and un assigned
		@param {Integer} idAlumno Student's id
	*/
	$scope.chargeCourses = function(idAlumno){
		$scope.cursos = []
		$scope.tempCursos = [];
		$scope.assignCursos = [];
		var data = [];
		ajaxHelper(mastUri+"curso", "GET").success(function(dat){
			//$scope.cursos = data;
			data = dat;
		});

		var asignados = [];
		ajaxHelper(mastUri+"alumno/course/"+idAlumno, "GET").success(function(dat){
			asignados = dat;
		});

		for(var asi in asignados){
			for(var cur in data){
				if(data[cur].nombre === asignados[asi].nombre){
					var index = data.indexOf(data[cur]);
					data.splice(index, 1);
					var assign = {
						nombre: asignados[asi].nombre,
						ciclo: asignados[asi].ciclo
					}
					$scope.tempCursos.push(assign);
				}
			}
		}
		$scope.cursos = data;

		$scope.idTemp = idAlumno;

		$("#modal-assignment").openModal();	
	}

	/**
		This function charges a course to list courses that will be assigned to student
		@param {Integer} idCurso Course's id
	*/
	$scope.chargeCourse = function(idcurso){
		var existe = false;
		for(var cur in $scope.assignCursos){
			if($scope.assignCursos[cur].idcurso === idcurso){
				existe = true;
				var index = $scope.assignCursos.indexOf($scope.assignCursos[cur]);
				$scope.assignCursos.splice(index, 1);
			}
		}
		if(existe === false){
			ajaxHelper(mastUri+"curso/"+idcurso, "GET").success(function(data){
				$scope.assignCursos.push(data);
			});
		}
	}

	/**
		This function assign course to student
	*/
	$scope.asignar = function(){
		var asign = {
			unidad1 : 0,
			unidad2 : 0,
			unidad3: 0,
			unidad4 : 0,
			unidad5 : 0,
			estado: "Perdio", 
			idalumno: $scope.idTemp,
			idcurso: 0
		}
		for(var asi in $scope.assignCursos){
			asign.idcurso = $scope.assignCursos[asi].idcurso;
			console.log(angular.toJson(asign));
			ajaxHelper(mastUri+"alumnocurso", "POST", asign).success(function(){});
		}
	}

	/**
		This function charge te user profile of student
		@param {Integer} idalumno Student's i
	*/
	$scope.viewmore = function(idAlumno){
		//alert("entro");
		sessionStorage.setItem("idalumno", idAlumno);
		chargeLogged();
		window.location.href = "/dashboardAlumno.html";
	}

	/**
		This function charge the user logged
	*/
	function chargeLogged(){
		ajaxHelper(mastUri+"alumno/"+sessionStorage["idalumno"],"GET").success(function(item){
			$scope.logged = item;
			ajaxHelper(mastUri+"alumno/course/"+sessionStorage["idalumno"], "GET").success(function(data){
					$scope.logged["notes"] = data
			});
		});
	}

	/**
		This function logout tre user logged
	*/
	$scope.logout = function(){
		sessionStorage.clear();	
		window.location.href = "/index.html";
	}

	/**
		This function delete a student
		@param {String} idAlumno Student's id to delete
	*/
	$scope.deleteAlumno = function(idAlumno){
		ajaxHelper(mastUri+"alumno/"+idAlumno, "DELETE").success(function(){
			for(var al in $scope.alumnos){
				//alert($scope.alumnos[al].idalumno+";"+ idAlumno);
				if($scope.alumnos[al].idalumno == idAlumno){
					var index = $scope.alumnos.indexOf($scope.alumnos[al]);
					$scope.alumnos.splice(index, 1);
				}
			}
		});
	}

	/**
		This function add a student
	*/
	$scope.agregarAlumno = function(){
		$scope.alumno.idmaestro = sessionStorage["id"];

		ajaxHelper(mastUri+"alumno", "POST", $scope.alumno).success(function(item){
			$scope.alumnos.push(item);
		});
		$scope.alumno = {
			nombre: "",
			password: "",
			correo: "",
			imagen: "",
			idmaestro: 0
		}
	}

	/**
		This function add a teacher
	*/
	$scope.agregarMaestro = function(){
		ajaxHelper(mastUri+"maestro", "POST", $scope.master).success(function(item){});
		$scope.master = {
			nombre: "",
			correo: "",
			password: ""
		}
	}

	/**
		This function add a course
	*/
	$scope.agregarCurso = function(){
		ajaxHelper(mastUri+"curso", "POST", $scope.course).success(function(item){});
		$scope.course = {
			nombre: "",
			ciclo: 0
		}
	}

});