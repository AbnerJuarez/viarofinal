CREATE DATABASE "dbFinal"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;

CREATE TABLE Maestro(
	idMaestro serial PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL,
	correo VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL
);

CREATE TABLE Alumno(
	idAlumno serial PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL,
	correo VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	imagen VARCHAR(255) NOT NULL,
	idMaestro INTEGER REFERENCES Maestro(idMaestro) match simple on update cascade on delete no action
);

CREATE TABLE Curso(
	idCurso serial PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL,
	ciclo INTEGER NOT NULL
);

CREATE TABLE AlumnoCurso(
	idAlumnoCurso serial PRIMARY KEY,
	unidad1 INTEGER NOT NULL,
	unidad2 INTEGER NOT NULL,
	unidad3 INTEGER NOT NULL,
	unidad4 INTEGER NOT NULL,
	unidad5 INTEGER NOT NULL,
	estado VARCHAR(255),
	idAlumno INTEGER REFERENCES Alumno(idAlumno) match simple on update cascade on delete no action,
	idCurso INTEGER REFERENCES Curso(idCurso) match simple on update cascade on delete no action
);

INSERT INTO Maestro(nombre, correo, password) VALUES 
	('Diego Illescas', 'dir@viaro.net', '123'),
	('Erick Juarez', 'er@viaro.net', '456'),
	('Inguelberth Garcia', 'ing@viaro.net', '789')

INSERT INTO Alumno(nombre, correo, password, imagen, idMaestro) VALUES
	('Andrea Sayes', 'andrea@viaro.net', '123', 'andrea.jpg', 1),
	('Kimberly Oddet', "odd@viaro.net", '123', 'oddet.jpg', 2),
	('Paula Lennon', 'pau@viaro.net', '123', 'pau.jpg', 3),
	('Rafael Herrarte', 'rafa@viaro.net', '456', 'rafa.jpg', 1),
	('Mario Valdez', 'mario@viaro.net', '456', 'mario.jpg', 2),
	('Manuel Perez', 'manuel@viaro.net', '456', 'manuel.jpg', 3)

INSERT INTO Curso(nombre, ciclo) VALUES ('Matematica', 2015), ('Idioma', 2015), ('Filosofia', 2015)

INSERT INTO AlumnoCurso(unidad1, unidad2, unidad3, unidad4, unidad5, estado, idAlumno, idCurso) VALUES
	(56,78,89,56,47,'',1,1),
	(89,78,84,71,89,'',1,2),
	(69,45,85,100,78,'',1,3),
	(56,78,89,56,47,'',2,1),
	(89,78,84,71,89,'',2,2),
	(69,45,85,100,78,'',2,3),
	(56,78,89,56,47,'',3,1),
	(89,78,84,71,89,'',4,2),
	(69,45,85,100,78,'',4,3),
	(56,78,89,56,47,'',5,1),
	(89,78,84,71,89,'',5,2),
	(69,45,85,100,78,'',5,3),
	(56,78,89,56,47,'',6,1),
	(89,78,84,71,89,'',6,2),
	(69,45,85,100,78,'',6,3)
