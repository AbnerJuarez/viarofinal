var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var pg = require('pg');
var connectionString = process.env.DATABASE_URL || 'postgres://postgres:123@localhost:5432/dbFinal';

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var url = '/final';

/*
	HERE THERE ARE ALL READ FUNCTIONS
*/

/**
	This router get registers of some entity in the data model
	@param {String} db_table Entity name
*/
router.get(url + "/:db_table", function(req, res){
	var table = req.params.db_table;

	console.log("GET");

	pg.connect(connectionString, function(err, client, done){
		queryPrint(err, client, done, res, table)
	});

});

/**
	This function get a register corresponding to id of some entity
	@param {String} db_table Entity name
	@param {Integer} db_id Id of register
*/
router.get(url + "/:db_table/:db_id", function(req, res){
	var table = req.params.db_table;
	var id = req.params.db_id;

	pg.connect(connectionString, function(err, client, done){
		queryPrintId(err, client, done, res, table, id);
	});
});

/**
	This function get all students of determinate teacher
	@param {Integer} db_idMaster Teacher's ID
*/
router.get(url + "/alumno/all/:db_idMaster", function(req, res){
	var table = req.params.db_table;
	var id = req.params.db_idMaster;
	var results = [];

	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT * FROM Alumno WHERE idmaestro="+id+" ORDER BY idalumno ASC");

		query.on('row', function(row){
			results.push(row);
		});

		query.on('end', function(){
			client.end();
			return res.json(results);
		});

		if(err){
			console.log(err);
		}
	});
});

/**
	This function get courses and notes of a student
*/
router.get(url + "/alumno/course/:db_idalumno", function(req, res){
	var id = req.params.db_idalumno;
	var results = [];
	
	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT alc.idalumnocurso, alc.unidad1, alc.unidad2, alc.unidad3, alc.unidad4, alc.unidad5, cur.nombre, cur.ciclo, alc.estado FROM AlumnoCurso alc INNER JOIN Curso cur ON cur.idcurso = alc.idCurso WHERE idalumno ="+id+" ORDER BY idalumnocurso ASC ");
		
		query.on('row', function(row){
			results.push(row);
		});

		query.on('end', function(){
			client.end();
			return res.json(results);
		});

		if(err){
			console.log(err);
		}
	});

});

/**
	This function verify that an user exist in database to permit his login
	@param {String} db_table Entity name to execute the query
	@return Return all information about the user
*/
///api/final/login/alumno?correo=pau@viaro.net&password=123
//api/final/login/maestro?correo=dir@viaro.net&password=123
router.post(url +"/login/:db_table", function(req, res){
	var table = String(req.params.db_table);
	var correo = req.query.correo;
	var password = req.query.password;
	var result = {} 

	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT * FROM "+ table + " WHERE correo='"+correo+"' AND password='"+password+"'");
		console.log("SELECT * FROM "+ table + " WHERE correo='"+correo+"' AND password='"+password+"'");

		query.on('row', function(row){
			result = row
		});

		query.on('end', function(){
			client.end();
			return res.json(result);
		});

		if(err){
			console.log(err);
		}
	});
});

/**
	This function print the query result of some table based on register's ID 
	@param {Object} err Error produced while the execute of query
	@param {Object} client Connection to the database
	@param {Object} done Status produced while the execute of query
	@param {Object} res Response of call to router
	@param {String} table Name entity
	@param {Integer} id Register's ID
	@return {JSON} Return the result of query
*/
function queryPrintId(err, client, done, res, table , id){
	var result = {};

	var query = client.query("SELECT * FROM "+ String(table) +
		" WHERE id" + String(table)+ "=" + parseInt(id) +"ORDER BY id"+String(table)+" ASC;");

	query.on('row', function(row){
		result = row;
	});

	query.on('end', function(){
		client.end();
		return res.json(result);
	});

	if(err){
		console.log(err);
	}
}

/**
	This function print the query result of some table
	@param {Object} err Error produced while the execute of query
	@param {Object} client Connection to the database
	@param {Object} done Status produced while the execute of query
	@param {Object} res Response of call to router
	@param {String} table Name entity
	@return {JSON} Return the results of query
*/
function queryPrint(err, client, done, res, table){
	var results = [];

	var query = client.query("SELECT * FROM "+ String(table) + " ORDER BY id"+String(table)+" ASC;");

	query.on('row', function(row){
		results.push(row);
	});

	query.on('end', function(){
		client.end();
		return res.json(results);
	});

	if(err){
		console.log(err);
	}
}

/*
	HERE THERE ARE ALL UPDATE FUNCTIONS
*/

/**
	This function update the information of a register in some entity
	@param {String} db_table Entity name
	@param {Integer} db_id Register's ID
	@return {JSON} Register after update
*/
router.put(url + "/:db_table/:db_id", function(req, res){
	var result = {};
	var table = req.params.db_table;
	var id = req.params.db_id;

	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT * FROM " + String(table) + " WHERE id"+ String(table) + 
			"="+String(id));

		query.on("row", function(row){
			result = row;
		});

		query.on("end", function(){
			var objeto = getObjectTable(table, req);
			//console.log(objeto);

			for(attr in result){
				if(objeto[attr] != null || objeto[attr] != undefined){
					if(result[attr] != objeto[attr]){
						console.log("El atributo " + result[attr] +":" + objeto[attr] + " es diferente");
						client.query("UPDATE "+table+" SET " + attr + "=($1)" +
							" WHERE id" + String(table) + "="+String(id), [objeto[attr]]);
						result[attr] = objeto[attr];
					}
				}
			}

			//client.end();
			return res.json(result);
		});

		if(err){
			console.log(err);
		}
	});
});

/**
	This function return an object filled by request switch a table
	@param {String} table Entity name
	@param {Object} Request to the router
*/
function getObjectTable(table, req){
	var objeto = {};

	switch(String(table)){
		case "maestro":
			objeto["nombre"] = req.body.nombre;
			objeto["correo"] = req.body.correo;
			objeto["password"] = req.body.password;
		break;
		case "alumno":
			objeto["nombre"] = req.body.nombre;
			objeto["correo"] = req.body.correo;
			objeto["password"] = req.body.password;
			objeto["imagen"] = req.body.imagen;
			objeto["idmaestro"] = parseInt(req.body.idmaestro);
		break;
		case "curso":
			objeto["nombre"] = req.body.nombre;
			objeto["ciclo"] = req.body.ciclo;
 		break;
		case "alumnocurso":
			objeto["unidad1"] = req.body.unidad1;
			objeto["unidad2"] = req.body.unidad2;
			objeto["unidad3"] = req.body.unidad3;
			objeto["unidad4"] = req.body.unidad4;
			objeto["unidad5"] = req.body.unidad5;
			objeto["estado"] = req.body.estado;
			objeto["idalumno"] = req.body.idalumno;
			objeto["idCurso"] = req.body.idcurso;
		break;
		default:
			objeto = null;
	}
	return objeto;
}

/*
	HERE THERE ARE ALL DELETE FUNCTIONS
*/
/**
	This function delete registers of some entity using an ID
	@param {String} db_table Entity name
	@param {Integer} db_id Register's ID
*/
router.delete(url + "/:db_table/:db_id", function(req, res){
	var table = String(req.params.db_table);
	var id = String(req.params.db_id);

	pg.connect(connectionString, function(err, client, done){
		client.query("DELETE FROM "+table+" WHERE id"+table+"="+id);

		queryPrint(err, client, done, res, table);
	});
});


/*
	HERE THERE ARE ALL INSERT FUNCTIONS
*/
/**
	This function insert registers in some entity
	@param {String} db_table Entity name
	@return {JSON} Return the register inserted
*/
router.post(url + "/:db_table", function(req, res){
	var table = String(req.params.db_table);
	var objeto = getObjectTable(table, req);
	var stquery = "INSERT INTO "+table+"(";
	var results = [];
	//console.log(objeto);

	pg.connect(connectionString, function(err, client, done){
		for(attr in objeto){
			if(stquery.indexOf('(') == (stquery.length-1)){
				stquery = stquery + attr;
			}else{
				stquery = stquery +", "+attr;
			}
		}
		stquery += ') VALUES(';
		var cont = 1;
		var data = [];
		for(attr in objeto){
			if(cont === 1){
				stquery += "$"+cont+"";
			}else{
				stquery = stquery +", " + "$"+cont+"";
			}
			cont++;
			data.push(objeto[attr]);
		}
		stquery += ');';
		//console.log(stquery);
		//console.log(data);

		client.query(stquery, data);

		var query = client.query("SELECT * FROM "+table+" ORDER BY id"+table+" DESC LIMIT 1");

		query.on("row", function(data){
			results.push(data);
		});

		query.on("end", function(){
			client.end();
			return res.json(results[0]);
		});

		if(err){
			console.log(err);
		}
	});
});


module.exports = router;